#!/usr/bin/env python
# coding: utf-8

# # Analysis of crime during Covid time
# #By- Aarush Kumar
# #Dated:November 13,2021

# In[13]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from glob import glob

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


# In[14]:


data = sorted(glob('/home/aarush100616/Downloads/Projects/Covid & Crime Analysis/Data/*metropolitan-street.csv'))

crimes = pd.concat((pd.read_csv(file).assign(filename = file)
                    for file in data), ignore_index = True)
crimes['Year'] =  crimes.Month.str.slice(0,4)
crimes['Month'] = crimes.Month.str.slice(5)
result = crimes['LSOA name'].str.rpartition()
crimes['Region'] = result[0]
crimes.drop(['Crime ID', 'Reported by','Falls within', 'Longitude', 'Latitude', 'Location', 'LSOA code', 'LSOA name', 'Last outcome category', 'Context', 'filename'], axis=1, inplace=True)
columns_headings = ['Month', 'Crime_Type', 'Year', 'Region']
crimes.columns = columns_headings

# Reorder the columns using '.reindex' 
neworder = ['Region','Year','Month','Crime_Type']
crimes=crimes.reindex(columns=neworder)


# In[15]:


with_region = crimes.Region.notna().sum()
without_region = crimes.Region.isna().sum()
total_crimes = crimes.Crime_Type.count()

percentage_missing = (without_region / total_crimes)
percentage_complete = (with_region / total_crimes)

fstring = f'Total Crimes: {total_crimes:,}\n'
fstring = fstring + f'Crimes with region: {with_region:,}\nCrimes missing region: {without_region:,}\n'
fstring = fstring  + f'Percentage missing Region: {percentage_missing:.2%}\n'
fstring = fstring  + f'Percentage with Region: {percentage_complete:.2%}'
print(fstring)


# In[16]:


# Count Crimes by Region then Sort Descending
# The top 33 values will (should) contain all the London Boroughs
region_summary = crimes.groupby(['Region']).Region.count()
sorted_region_summary = region_summary.sort_values(ascending=False)
data = sorted_region_summary.head(33)
boroughs = data.index.to_list()


# In[17]:


# Create one dataframe for Met Crimes and one for all others
met_crimes = crimes.loc[crimes.Region.isin(boroughs)]
non_met_crimes = crimes.loc[~crimes.Region.isin(boroughs)]


# In[18]:


year = '2020'
crime = 'Anti-social behaviour'

plot_data = met_crimes.loc[(met_crimes.Year==year)&(met_crimes.Crime_Type==crime), :].groupby(['Region','Month']).Crime_Type.count().reset_index()
plot_data.columns=['Region', 'Month', 'Count']
plot_data.head()


# In[19]:


df = plot_data.pivot(index='Month', columns='Region', values='Count')
df.head()


# In[20]:


df2 = plot_data.pivot(index='Region', columns='Month', values='Count')
df2.head()


# In[21]:


import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

plt.rcParams['figure.figsize'] = (25,20)
# plt.rcParams['legend.loc'] = 1
plt.xticks(fontsize = 20)
plt.yticks(fontsize = 20)

sns.set(font_scale = 10)
sns.set_theme(style="ticks")
sns.set_context("paper")
sns.color_palette("Spectral", as_cmap=True)

mychart = sns.lineplot(data=df, linewidth=1.5, dashes=False)
mychart.set_title((crime + ' by Month ' + '(' + year + ')'), fontsize = 25)
mychart.set_xlabel("Month", fontsize = 25)
mychart.set_ylabel("Count", fontsize = 25)

# mychart.legend(loc = 2, bbox_to_anchor= (1,.75,0.1,0.1), fontsize = 15, shadow = True )
mychart.legend(loc = 2,  bbox_to_anchor= (0.001,1), fontsize = 13, shadow = True )


# In[22]:


import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

plt.rcParams['figure.figsize'] = (25,20)
# plt.rcParams['legend.loc'] = 1
plt.xticks(fontsize = 20)
plt.yticks(fontsize = 20)

sns.set(font_scale = 10)
sns.set_theme(style="darkgrid")
sns.set_context("paper")

mychart = sns.boxplot(data=df2, palette='Paired')
mychart.set_title((crime + ' by Month ' + '(' + year + ')'), fontsize = 25)
mychart.set_xlabel("Month", fontsize = 25)
mychart.set_ylabel("Count", fontsize = 25)

